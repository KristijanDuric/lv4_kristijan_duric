﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data = new Dataset("test.txt");
            Analyzer3rdParty analyze = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyze);
            Console.WriteLine(adapter.CalculateAveragePerColumn(data));

            double[] res = adapter.CalculateAveragePerColumn(data);

            foreach (double r in res)
            {
                Console.WriteLine(r);
            }
        }
    }
}
