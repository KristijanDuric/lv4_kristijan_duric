﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    interface IEmailValidatorService
    {
        bool IsValidAddress(String candidate);
    }
}
